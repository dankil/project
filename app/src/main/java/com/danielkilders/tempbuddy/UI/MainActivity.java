package com.danielkilders.tempbuddy.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.danielkilders.tempbuddy.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SELECTED_EMPLOYEE_ID = "SELECTED_EMPLOYEE_ID";

    Button mHommerButton, mHarryButton, mVaderButton, mJohnButton, mPeterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get layout views
        mHarryButton = findViewById(R.id.harryButton);
        mHommerButton = findViewById(R.id.hommerButton);
        mVaderButton = findViewById(R.id.vaderButton);
        mJohnButton = findViewById(R.id.johnButton);
        mPeterButton = findViewById(R.id.peterButton);

        // Define onclicklistener that will be shared among the buttons
        View.OnClickListener nextActivityListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // define intent
                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);

                // get the clicked buttons TAG - it contains the ID of the profile we have to request
                String buttonTag = view.getTag().toString();

                // make the tag available in the next activity through the intent
                intent.putExtra(SELECTED_EMPLOYEE_ID, buttonTag);

                startActivity(intent);
            }
        };

        // Add the custom listener to the views
        mHarryButton.setOnClickListener(nextActivityListener);
        mHommerButton.setOnClickListener(nextActivityListener);
        mVaderButton.setOnClickListener(nextActivityListener);
        mJohnButton.setOnClickListener(nextActivityListener);
        mPeterButton.setOnClickListener(nextActivityListener);

    }

}
