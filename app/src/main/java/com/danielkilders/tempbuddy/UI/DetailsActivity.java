package com.danielkilders.tempbuddy.UI;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.danielkilders.tempbuddy.R;
import com.danielkilders.tempbuddy.model.Employee;
import com.danielkilders.tempbuddy.model.Interval;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DetailsActivity extends AppCompatActivity {

    public static final String EMPLOYEE = "EMPLOYEE";
    private static final String TAG = DetailsActivity.class.getSimpleName();
    private static final String BASE_API = "https://staging.tempbuddy.com/public/api/jobs";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TextView mToolbarTitle;
    private ActionBar mActionBar;
    private Bundle mBundle;

    private String mEmployeeName, buttonTag;
    private List<Interval> mJobs;
    private List<Interval> mBreaks;
    private Employee mEmployee;


    private Fragment mPayslipFragment;
    private Fragment mJobsListFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.container);
        Toolbar toolbar = findViewById(R.id.toolbar);

        // Initialize variables
        mJobs = new ArrayList<Interval>();
        mBreaks = new ArrayList<Interval>();
        mBundle = new Bundle();

        // Retrieve employee ID that was passed via the intent
        Intent intent = getIntent();
        buttonTag = intent.getStringExtra(MainActivity.SELECTED_EMPLOYEE_ID);
        Log.v(TAG, "Employee Tag: " + buttonTag);

        // set custom toolbar as action bar and retrieve title textview
        mToolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);

        // Make sure actionBar does not have a title
        mActionBar = getSupportActionBar();
        mActionBar.setTitle("");

        // Create the adapter that will return a fragment for each section
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        fetchData();
    }

    private void fetchData() {
        String URL = BASE_API + "?userId=" + buttonTag;
        Log.v(TAG, "Request URl: " + URL);

        if (isNetworkAvailable()) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(URL)
                    .build();

            Call call = client.newCall(request);

            // handle request asynchronously
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String stringResponse = response.body().string();
                    try {
                        JSONObject jsonObject = new JSONObject(stringResponse);
                        Log.v(TAG, "Response object: " + jsonObject.toString());

                        mEmployeeName = jsonObject.getString("name");

                        JSONArray jobs = jsonObject.getJSONArray("jobs");
                        for (int i=0; i < jobs.length(); i++) {
                            JSONObject jobObject = jobs.getJSONObject(i);
                            String startDate = jobObject.getString("start");
                            String endDate = jobObject.getString("end");

                            Interval job = new Interval(startDate, endDate);
                            mJobs.add(job);
                        }

                        JSONArray breaks = jsonObject.getJSONArray("breaks");
                        for (int i=0; i < breaks.length(); i++) {
                            JSONObject breakObject = breaks.getJSONObject(i);
                            String startDate = breakObject.getString("start");
                            String endDate = breakObject.getString("end");

                            Interval aBreak = new Interval(startDate, endDate);
                            mBreaks.add(aBreak);
                        }

                        mEmployee = new Employee(buttonTag, mEmployeeName, mJobs, mBreaks);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mToolbarTitle.setText(mEmployee.getName());

                                // Set up the ViewPager with the sections adapter.
                                mViewPager.setAdapter(mSectionsPagerAdapter);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call call, IOException e) {
                    Toast.makeText(getApplicationContext(), "Something went wrong with the server", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Request failed");
                }
            });
        } else {
            Log.e(TAG, "No internet");
        }



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the custom menu
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks
        // We have no items in menu, so we don't need this
        return super.onOptionsItemSelected(item);
    }

    /*
     * Check if the phone has internet connection
     */
    private boolean isNetworkAvailable() {
        boolean isAvailable = false;

        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private int NUM_ITEMS = 2;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            mBundle.putInt("DOWNLOAD", 0);
            switch (position) {
                case 0: // JobsList Fragment
                    mJobsListFragment = JobsListFragment.newInstance(mEmployee);
                    return mJobsListFragment;
                case 1: // PayslipFragment fragment
                    mPayslipFragment = PayslipFragment.newInstance(mEmployee);
                    return mPayslipFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return NUM_ITEMS;
        }
    }
}
