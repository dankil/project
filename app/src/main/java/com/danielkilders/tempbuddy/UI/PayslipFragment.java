package com.danielkilders.tempbuddy.UI;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.danielkilders.tempbuddy.R;
import com.danielkilders.tempbuddy.model.Employee;
import com.danielkilders.tempbuddy.model.Interval;
import com.danielkilders.tempbuddy.utils.ShiftsManager;
import com.danielkilders.tempbuddy.utils.TimeConverter;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Daniel K on 28/03/2018.
 */

public class PayslipFragment extends Fragment {

    private static final String TAG = PayslipFragment.class.getSimpleName();
    private Employee mEmployee;

    private Button mCalculateWithBreaks, mCalculateWithoutBreaks;
    private TextView mWorkedHoursTextView, mBreakHoursTextView, mTotalBreakTextView, mPayslipAmount;

    private int payOfWork;
    private int payOfBreaks;

    public PayslipFragment() {

    }

    public static PayslipFragment newInstance(Parcelable parcelable ) {
        PayslipFragment fragment = new PayslipFragment();
        Bundle args = new Bundle();
        args.putParcelable(DetailsActivity.EMPLOYEE, parcelable);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payslip, container, false);

        mWorkedHoursTextView = rootView.findViewById(R.id.totalWorkedHoursTextView);
        mBreakHoursTextView = rootView.findViewById(R.id.totalBreakHoursTextView);
        mTotalBreakTextView = rootView.findViewById(R.id.totalWorkHoursTextView);
        mCalculateWithBreaks = rootView.findViewById(R.id.calculateWithBreakButton);
        mCalculateWithoutBreaks = rootView.findViewById(R.id.calculateWithoutBreakButton);
        mPayslipAmount = rootView.findViewById(R.id.payslipAmountTextView);

        // retrieve bundle and Employee data
        Bundle args = getArguments();
        mEmployee = args.getParcelable(DetailsActivity.EMPLOYEE);

        ShiftsManager shiftsManager = new ShiftsManager();
        TimeConverter timeConverter = new TimeConverter();

        long workedMinutes = shiftsManager.getMonthlyInterval(mEmployee.getJobs());
        mWorkedHoursTextView.setText(timeConverter.timeToString(workedMinutes));

        long breakMinutes = shiftsManager.getMonthlyInterval(mEmployee.getBreaks());
        mBreakHoursTextView.setText(timeConverter.timeToString(breakMinutes));

        long totalMinutes = shiftsManager.getTotalHoursAtWork(mEmployee);
        mTotalBreakTextView.setText(timeConverter.timeToString(totalMinutes));

        List<Interval> jobs = mEmployee.getJobs();
        List<Interval> breaks = mEmployee.getBreaks();
        Iterator jobIterator = jobs.iterator();
        Iterator breakIterator = breaks.iterator();

        // both iterations could have been done all at once in previous fragment and then added to mEmployee
        // or done in a separate thread, instead of UI thread
        // however, for now it's not hurting UX
        payOfWork = 0;
        while (jobIterator.hasNext()) {
            Interval job = (Interval) jobIterator.next();
            payOfWork += (int) shiftsManager.getShiftPay(job);
        }

        payOfBreaks = 0;
        while (breakIterator.hasNext()) {
            Interval aBreak = (Interval) breakIterator.next();
            payOfBreaks += (int) shiftsManager.getShiftPay(aBreak);
        }

        mCalculateWithoutBreaks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPayslipAmount.setText(payOfWork + "€");
            }
        });

        mCalculateWithBreaks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int total = payOfBreaks + payOfWork;
                mPayslipAmount.setText(total + "€");
            }
        });

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
}