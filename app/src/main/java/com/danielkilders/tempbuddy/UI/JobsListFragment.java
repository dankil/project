package com.danielkilders.tempbuddy.UI;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.danielkilders.tempbuddy.R;
import com.danielkilders.tempbuddy.adapter.JobsAdapter;
import com.danielkilders.tempbuddy.model.Employee;
import com.danielkilders.tempbuddy.model.Interval;

import java.util.Calendar;
import java.util.List;


/**
 * Created by Daniel K on 28/03/2018.
 */

public class JobsListFragment extends Fragment {

    private static final String TAG = JobsListFragment.class.getSimpleName();

    private Employee mEmployee;
    private RecyclerView mRecyclerView;
    private JobsAdapter mAdapter;
    private List<Interval> mJobs;
    private LinearLayout mAddItem;

    public JobsListFragment() {

    }

    public static JobsListFragment newInstance(Parcelable parcelable) {
        JobsListFragment fragment = new JobsListFragment();
        Bundle args = new Bundle();
        args.putParcelable(DetailsActivity.EMPLOYEE, parcelable);
        fragment.setArguments(args);
        Log.v(TAG, "created!");
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jobs_list, container, false);
        mRecyclerView = rootView.findViewById(R.id.jobsRecyclerView);
        mAddItem = rootView.findViewById(R.id.addItemLinearLayout);

        // retrieve bundle and Employee data
        Bundle args = getArguments();
        mEmployee = args.getParcelable(DetailsActivity.EMPLOYEE);

        mJobs = mEmployee.getJobs();
        mAdapter = new JobsAdapter(mJobs);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(mAdapter);

        mAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View customView = inflater.inflate(R.layout.dialog_layout, null);
                Button dateStartPicker = customView.findViewById(R.id.dateStartPickerButton);
                TextView dateStartTextView = customView.findViewById(R.id.chosenDateTextView);
                Button startHourPicker = customView.findViewById(R.id.startHourPickerButton);
                TextView startHoursTextView = customView.findViewById(R.id.chosenHoursTextView);
                Button dateEndPicker = customView.findViewById(R.id.dateEndPickerButton);
                TextView dateEndTextView = customView.findViewById(R.id.chosenEndDateTextView);
                Button endHourPicker = customView.findViewById(R.id.endHourPickerButton);
                TextView endHourTextView = customView.findViewById(R.id.chosenEndHourTextView);

                // retrieved data from dialogs will be saved in this arrays
                final int[] startDate = new int[5];
                final int[] endDate = new int[5];

                // create the dialog
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                        .setTitle("Add a job!")
                        .setView(customView)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Adding data to mJobs list - quick method without respecting timeframes
                                Interval interval = new Interval(
                                        startDate[0] + "-" + startDate[1] + "-" + startDate[2] + "T" + startDate[3] + ":" + startDate[4] + "+00:00",
                                        endDate[0] + "-" + endDate[1] + "-" + endDate[2] + "T" + endDate[3] + ":" + endDate[4] + "+00:00");
                                mJobs.add(interval);
                                mAdapter.notifyItemInserted(mJobs.size() - 1);
                                mAdapter.notifyDataSetChanged();
                            }
                        });

                // create the starting day dialog
                Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener dateStart = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startDate[0] = year;
                        startDate[1] = monthOfYear + 1;
                        startDate[2] = dayOfMonth;
                        dateStartTextView.setText(dayOfMonth + " " + startDate[1] + " " + year);
                    }
                };
                dateStartPicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DatePickerDialog(getContext(), dateStart, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });

                // create the starting hour dialog
                startHourPicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                                startHoursTextView.setText(hourOfDay + ":" + minute + " hours");
                                startDate[3] = hourOfDay;
                                startDate[4] = minute;
                            }
                        }, myCalendar.get(Calendar.HOUR_OF_DAY),
                                myCalendar.get(Calendar.MINUTE),
                                false).show();
                    }
                });

                // create the ending date dialog
                DatePickerDialog.OnDateSetListener dateEnd = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        endDate[0] = year;
                        endDate[1] = monthOfYear + 1;
                        endDate[2] = dayOfMonth;
                        dateEndTextView.setText(dayOfMonth + " " + endDate[1] + " " + year);
                    }
                };
                dateEndPicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DatePickerDialog(getContext(), dateEnd, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });

                // create ending hour dialog
                endHourPicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                                endHourTextView.setText(hourOfDay + ":" + minute + " hours");
                                endDate[3] = hourOfDay;
                                endDate[4] = minute;
                            }
                        }, myCalendar.get(Calendar.HOUR_OF_DAY),
                                myCalendar.get(Calendar.MINUTE),
                                false).show();
                    }
                });

                dialog.show();
            }
        });

        return rootView;
    }
}
