package com.danielkilders.tempbuddy.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.danielkilders.tempbuddy.R;
import com.danielkilders.tempbuddy.model.Interval;
import com.danielkilders.tempbuddy.utils.ShiftsManager;
import com.danielkilders.tempbuddy.utils.TimeConverter;

import java.util.List;


/**
 * Created by Daniel K on 29/03/2018.
 */

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.ViewHolder> {

    private static final String TAG = JobsAdapter.class.getSimpleName();

    private List<Interval> mJobs;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mDateTextView, mHoursWorkedTextView, mPayedAmount;
        private ImageButton mDeleteButton;

        public ViewHolder(View itemView) {
            super(itemView);

            mDateTextView = itemView.findViewById(R.id.jobDateTextView);
            mHoursWorkedTextView = itemView.findViewById(R.id.jobHoursWorkedTextView);
            mPayedAmount = itemView.findViewById(R.id.jobPayedAmount);
            mDeleteButton = itemView.findViewById(R.id.deleteButton);
        }
    }

    public JobsAdapter(List<Interval> jobsList) { mJobs = jobsList; }

    @Override
    public JobsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_recyclerview_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobsAdapter.ViewHolder holder, int position) {
        ShiftsManager shiftsManager = new ShiftsManager();

        Interval job = mJobs.get(position);
        holder.mDateTextView.setText(shiftsManager.getShiftStartDay(job));

        TimeConverter timeConverter = new TimeConverter();
        holder.mHoursWorkedTextView.setText(timeConverter.timeToString(shiftsManager.getShiftInterval(job)));

        int totalAmount = (int) shiftsManager.getShiftPay(job);
        holder.mPayedAmount.setText(totalAmount + " €");

        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mJobs.remove(position);
                notifyDataSetChanged();
                notifyItemRangeChanged(position, mJobs.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mJobs.size();
    }

}
