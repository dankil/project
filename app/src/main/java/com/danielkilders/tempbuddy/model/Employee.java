package com.danielkilders.tempbuddy.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel K on 29/03/2018.
 */

public class Employee implements Parcelable{

    private String mUserId;
    private String mName;
    private List<Interval> mJobs;
    private List<Interval> mBreaks;

    public Employee(String userId, String name, List<Interval> jobs, List<Interval> breaks) {
        mUserId = userId;
        mName = name;
        mJobs = jobs;
        mBreaks = breaks;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getName() {
        return mName;
    }

    public List<Interval> getJobs() {
        return mJobs;
    }

    public List<Interval> getBreaks() {
        return mBreaks;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    // Make the Employee class parcelable, in order to be able to send it from activity to activity
    // has be written in the same order as retrieved
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mUserId);
        parcel.writeString(mName);
        parcel.writeList(mJobs);
        parcel.writeList(mBreaks);
    }

    private Employee(Parcel in) {
        mUserId = in.readString();
        mName = in.readString();
        mJobs = new ArrayList<Interval>();
        in.readList(mJobs, Interval.class.getClassLoader());
        mBreaks = new ArrayList<Interval>();
        in.readList(mBreaks, Interval.class.getClassLoader());
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel parcel) {
            return new Employee(parcel);
        }

        @Override
        public Employee[] newArray(int i) {
            return new Employee[i];
        }
    };
}
