package com.danielkilders.tempbuddy.model;

/**
 * Created by Daniel K on 29/03/2018.
 */

public class Interval {

    private String mStarts;
    private String mEnds;

    public Interval(String starts, String ends) {
        mStarts = starts;
        mEnds = ends;
    }

    public String getStarts() {
        return mStarts;
    }

    public String getEnds() {
        return mEnds;
    }

}