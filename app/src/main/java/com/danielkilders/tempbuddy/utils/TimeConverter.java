package com.danielkilders.tempbuddy.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by Daniel K on 29/03/2018.
 */

public class TimeConverter {
    public String timeToString(long timeInMinutes) {
        long hours = TimeUnit.MINUTES.toHours(timeInMinutes);
        long minutes = timeInMinutes % 60;

        if (minutes < 10) {
            return hours + ":0" + minutes + " hours";
        } else {
            return hours + ":" + minutes + " hours";
        }
    }
}
