package com.danielkilders.tempbuddy.utils;


import android.util.Log;

import com.danielkilders.tempbuddy.model.Employee;
import com.danielkilders.tempbuddy.model.Interval;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Daniel K on 29/03/2018.
 */

public class ShiftsManager {

    private static final String TAG = ShiftsManager.class.getSimpleName();

    private SimpleDateFormat mSimpleDateFormat;
    SimpleDateFormat mOutput;

    public ShiftsManager() {
        mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        mOutput = new SimpleDateFormat("HH:mm");
    }

    // Get minutes in a list of intervals
    public long getMonthlyInterval(List<Interval> intervalList) {
        long totalIntervalMinutes = 0;
        Iterator iterator = intervalList.iterator();

        while (iterator.hasNext()) {
            Interval interval = (Interval) iterator.next();
            totalIntervalMinutes += getShiftInterval(interval);
        }
        return  totalIntervalMinutes;
    }

    // get minutes in a single shift
    public long getShiftInterval(Interval interval) {
        long intervalMinutes = 0;
        try {
            // get starting and ending hours
            Date startDate = mSimpleDateFormat.parse(interval.getStarts());
            Date endDate = mSimpleDateFormat.parse(interval.getEnds());

            // calculate difference
            long intervalTime = endDate.getTime() - startDate.getTime();

            // convert the difference to minutes
            intervalMinutes = ((intervalTime / 1000) / 60) ;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return intervalMinutes;
    }

    // get total minutes of jobs + breaks
    public long getTotalHoursAtWork(Employee employee) {
        long totalTime = 0;
        totalTime += getMonthlyInterval(employee.getJobs());
        totalTime += getMonthlyInterval(employee.getBreaks());
        return totalTime;
    }

    // get the day where the shift starts
    public String getShiftStartDay(Interval interval) {
        SimpleDateFormat jobListDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        Date date = new Date();
        try {
            date = mSimpleDateFormat.parse(interval.getStarts());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jobListDateFormat.format(date);
    }

    public double getShiftPay(Interval interval) {
        // money the user will get
        double pay = 0;

        // Money earned per minute in each shift
        double morningShiftPay = 8.0 / 60;
        double eveningShiftPay = 10.5 / 60;
        double nightShiftPay = 12.0 / 60;

        try {
            // get the hour when the user started the shift in minutes
            String fulldateStart = interval.getStarts();
            Date inputStartDate = mSimpleDateFormat.parse(fulldateStart);
            String inputStartString = mOutput.format(inputStartDate);
            long startDate = getHourInMinutes(inputStartString);

            // get pay timeframes hours in minutes
            long cero = getHourInMinutes("00:00");
            long six = getHourInMinutes("06:59");
            long seven = getHourInMinutes("07:00");
            long fourteen = getHourInMinutes("14:59");
            long fifteen = getHourInMinutes("15:00");
            long twentytwo =  getHourInMinutes("22:59");
            long twentythree = getHourInMinutes("23:00");
            long twentyfour = getHourInMinutes("24:00");

            // number of minutes the user was in the shift
            long usersShift = getShiftInterval(interval);

            // helper variable used to calculate the minutes in each paying timeframe
            long minutes = 0;

            // iterate until usershift reaches 0
            // check current timeframe to assign proper payment
            // subtract current calculated timeframe from usershift and asign upper timeframe value to startDate to be used as new bottom timeframe
            while (usersShift != 0) {
                if (startDate >= twentythree && startDate <= twentyfour) {
                    if (usersShift > 60) {
                        pay += 60 * nightShiftPay;
                        usersShift -= 60;
                        startDate = cero;
                    } else {
                        pay += usersShift * nightShiftPay;
                        usersShift = 0;
                    }
                } else if (startDate >= fifteen && startDate <= twentytwo) {
                    if ((startDate + usersShift) > twentytwo) {
                        minutes = twentytwo - startDate;
                        pay += minutes * eveningShiftPay;
                        usersShift -= minutes;
                        startDate = twentythree;
                    } else {
                        pay += usersShift * eveningShiftPay;
                        usersShift = 0;
                    }
                } else if (startDate >= seven && startDate <= fourteen) {
                    if ((startDate + usersShift) > fourteen) {
                        minutes = fourteen - startDate;
                        pay += minutes * morningShiftPay;
                        usersShift -= minutes;
                        startDate = fifteen;
                    } else {
                        pay += usersShift * morningShiftPay;
                        usersShift = 0;
                    }
                } else if (startDate >= cero && startDate <= six) {
                    if ((startDate + usersShift) > six) {
                        minutes = six - startDate;
                        pay += minutes * nightShiftPay;
                        usersShift -= minutes;
                        startDate = seven;
                    } else {
                        pay += usersShift * nightShiftPay;
                        usersShift = 0;
                    }
                }
            }
            } catch(ParseException e){
                e.printStackTrace();
            }
        return Math.ceil(pay);
    }

    /*
    * Transform given time pattern into minutes
    */
    private long getHourInMinutes(String string) throws ParseException {
        Date date = mOutput.parse(string);
        return date.getTime() / 1000 / 60; // return in minutes
    }
}
